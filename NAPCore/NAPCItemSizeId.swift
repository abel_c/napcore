//
//  NAPCItemSizeId.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public enum NAPCItemSizeId : String {
  case xxs = "00002_XXS_Clothing"
  case xs = "00003_XS_Clothing"
  case s = "00004_S_Clothing"
  case m = "00005_M_Clothing"
  case l = "00006_L_Clothing"
  case xl = "00007_XL_Clothing"
  case xxl = "00008_XXL_Clothing"
  
  public static func getSet(array: [String]) -> Set<NAPCItemSizeId> {
    var set = Set<NAPCItemSizeId>()
    array.forEach { (itemSizeString) in
      if let itemSize = NAPCItemSizeId(rawValue: itemSizeString) {
        set.insert(itemSize)
      }
    }
    return set
  }
}
