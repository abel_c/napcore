//
//  NAPCItem.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public protocol NAPCItemProtocol {
  var name: String {get}
  var visible: Bool {get}
  var saleableStandardSizeIds: Set<NAPCItemSizeId> {get}
  var price: NAPCPriceProtocol {get}
  var leafCategoryIds: [Int] {get}
  var onSale: Bool {get}
  var analyticsKey: String {get}
  var id: Int {get}
  var brandId: Int {get}
  var colourIds: [Int] {get}
  var images: NAPCImageProtocol {get set}
  var badges: Set<NAPCBadge> {get}
}
public extension NAPCItemProtocol {
  static func get(name: String = "name", visible: Bool = true, saleableStandardSizeIds: Set<NAPCItemSizeId> = Set<NAPCItemSizeId>([NAPCItemSizeId.m]), price: NAPCPriceProtocol = NAPCPrice.get(), leafCategoryIds: [Int] = [1], onSale: Bool = false, analyticsKey: String = "analyticsKey", id: Int = 999, brandId: Int = 1, colourIds: [Int] = [1], images: NAPCImageProtocol = NAPCImage.get(), badges: Set<NAPCBadge> = Set<NAPCBadge>([NAPCBadge.Exclusive])) -> NAPCItemProtocol{
    return NAPCItem(name: name, visible: visible, saleableStandardSizeIds: saleableStandardSizeIds, price: price, leafCategoryIds: leafCategoryIds, onSale: onSale, analyticsKey: analyticsKey, id: id, brandId: brandId, colourIds: colourIds, images: images, badges: badges)
  }
}

public struct NAPCItem : NAPCItemProtocol {
  
  fileprivate struct Keys {
    static let name = "name"
    static let visible = "visible"
    static let saleableStandardSizeIds = "saleableStandardSizeIds"
    static let price = "price"
    static let leafCategoryIds = "leafCategoryIds"
    static let onSale = "onSale"
    static let analyticsKey = "analyticsKey"
    static let id = "id"
    static let brandId = "brandId"
    static let colourIds = "colourIds"
    static let images = "images"
    static let badges = "badges"
  }
  
  public var name: String
  public var visible: Bool
  public var saleableStandardSizeIds: Set<NAPCItemSizeId>
  public var price: NAPCPriceProtocol
  public var leafCategoryIds: [Int]
  public var onSale: Bool
  public var analyticsKey: String
  public var id: Int
  public var brandId: Int
  public var colourIds: [Int]
  public var images: NAPCImageProtocol
  public var badges: Set<NAPCBadge>
  
  public init?(dictionary: [String:Any]) {
    
    if  let name = dictionary[Keys.name] as? String,
      let visible = dictionary[Keys.visible] as? Bool,
      let saleableStandardSizeIds = dictionary[Keys.saleableStandardSizeIds] as? [String],
      let priceDict = dictionary[Keys.price] as? [String:Any],
      let price = NAPCPrice(dictionary: priceDict),
      let leafCategoryIds = dictionary[Keys.leafCategoryIds] as? [Int],
      let onSale = dictionary[Keys.onSale] as? Bool,
      let analyticsKey = dictionary[Keys.analyticsKey] as? String,
      let id = dictionary[Keys.id] as? Int,
      let brandId = dictionary[Keys.brandId] as? Int,
      let colourIds = dictionary[Keys.colourIds] as? [Int],
      let imagesDict = dictionary[Keys.images] as? [String:Any],
      let images = NAPCImage(dictionary: imagesDict),
      let badges = dictionary[Keys.badges] as? [String] {
      
      self.name = name
      self.visible = visible
      self.saleableStandardSizeIds = NAPCItemSizeId.getSet(array: saleableStandardSizeIds)
      self.price = price
      self.leafCategoryIds = leafCategoryIds
      self.onSale = onSale
      self.analyticsKey = analyticsKey
      self.id = id
      self.brandId = brandId
      self.colourIds = colourIds
      self.images = images
      self.badges = NAPCBadge.getSet(array: badges)
    }
    else {
      return nil
    }
  }
  
  // Testing
  public init(name: String, visible: Bool, saleableStandardSizeIds: Set<NAPCItemSizeId>, price: NAPCPriceProtocol, leafCategoryIds: [Int], onSale: Bool, analyticsKey: String, id: Int, brandId: Int, colourIds: [Int], images: NAPCImageProtocol, badges: Set<NAPCBadge>) {
    self.name = name
    self.visible = visible
    self.saleableStandardSizeIds = saleableStandardSizeIds
    self.price = price
    self.leafCategoryIds = leafCategoryIds
    self.onSale = onSale
    self.analyticsKey = analyticsKey
    self.id = id
    self.brandId = brandId
    self.colourIds = colourIds
    self.images = images
    self.badges = badges
  }
  
}
