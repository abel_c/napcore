//
//  NAPCImage.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public enum Scheme : String {
  case https = "https:"
}

public enum NAPCImageShot : String {
  case _in = "in"
  case ou = "ou"
  case fr = "fr"
  case bk = "bk"
  case cu = "cu"
  
  public static func getSet(array: [String]) -> Set<NAPCImageShot> {
    var set = Set<NAPCImageShot>()
    array.forEach { (imageShotString) in
      if let imageShot = NAPCImageShot(rawValue: imageShotString) {
        set.insert(imageShot)
      }
    }
    return set
  }
}

public enum NAPCImageSize : String {
  case dl, l, m, m2, mt, mt2, pp, s, sl, xl, xs, xxl
  
  public static func getSet(array: [String]) -> Set<NAPCImageSize> {
    var set = Set<NAPCImageSize>()
    array.forEach { (imageSizeString) in
      if let imageSize = NAPCImageSize(rawValue: imageSizeString) {
        set.insert(imageSize)
      }
    }
    return set
  }
}

public protocol NAPCImageProtocol {
  var shots: Set<NAPCImageShot>  {get}//["in","ou","fr","bk","cu"],"
  var sizes: Set<NAPCImageSize> {get} //["dl","l","m","m2","mt","mt2","pp","s","sl","xl","xs","xxl"],"
  var mediaType: String {get} //"image/jpeg","
  var urlTemplate: String {get}//"{{scheme}}//cache.net-a-porter.com/images/products/991828/991828_{{shot}}_{{size}}.jpg"},"badges":["Exclusive","In_Stock"
  
  func imageURL(scheme: Scheme, shot: NAPCImageShot, size: NAPCImageSize) -> URL?
}

public extension NAPCImageProtocol {
  static func get() -> NAPCImageProtocol {
    return NAPCImage(shots: Set<NAPCImageShot>([NAPCImageShot._in]), sizes:  Set<NAPCImageSize>([NAPCImageSize.m]), mediaType: "jpeg", urlTemplate: "{{scheme}}//cache.net-a-porter.com/images/products/942725/942725_{{shot}}_{{size}}.jpg")
  }
}
public extension NAPCImageProtocol {
  func imageURL() -> URL? {
    return self.imageURL(scheme: .https, shot: ._in, size: .s)
  }
  
}
public struct NAPCImage : NAPCImageProtocol {
  fileprivate struct Keys {
    static let shots = "shots"
    static let sizes = "sizes"
    static let mediaType = "mediaType"
    static let urlTemplate = "urlTemplate"
  }
  public var shots: Set<NAPCImageShot>
  public var sizes: Set<NAPCImageSize>
  public var mediaType: String
  public var urlTemplate: String
  
  public init?(dictionary: [String:Any]) {
    if  let shots = dictionary[Keys.shots] as? [String],
      let sizes = dictionary[Keys.sizes] as? [String],
      let mediaType = dictionary[Keys.mediaType] as? String,
      let urlTemplate = dictionary[Keys.urlTemplate] as? String {
      
      self.shots = NAPCImageShot.getSet(array: shots)
      self.sizes = NAPCImageSize.getSet(array: sizes)
      self.mediaType = mediaType
      self.urlTemplate = urlTemplate
    }
    else {
      return nil
    }
  }
  
  // Testing
  public init(shots: Set<NAPCImageShot>, sizes: Set<NAPCImageSize>, mediaType: String, urlTemplate: String) {
    self.shots = shots
    self.sizes = sizes
    self.mediaType = mediaType
    self.urlTemplate = urlTemplate
  }
  public func defaultImageURL() -> URL? {
    return imageURL()
  }

  public func imageURL(scheme: Scheme = .https, shot: NAPCImageShot = ._in, size: NAPCImageSize = .s) -> URL? {
    // "{{scheme}}//cache.net-a-porter.com/images/products/942725/942725_{{shot}}_{{size}}.jpg"
    return URL(string: urlTemplate.replacingOccurrences(of: "{{scheme}}", with: scheme.rawValue).replacingOccurrences(of: "{{shot}}", with: shot.rawValue).replacingOccurrences(of: "{{size}}", with: size.rawValue))
  }
}
