//
//  NAPCPrice.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public protocol NAPCPriceProtocol {
  var currency: String {get} //":"GBP","
  var divisor: Int {get} //":100,"
  var amount: Int {get} //":23000},"
  init?(dictionary: [String:Any])
}

public extension NAPCPriceProtocol {
  static func get(currency: String = "GBP", divisor: Int = 100, amount: Int = 40000) -> NAPCPriceProtocol {
    return NAPCPrice(currency: currency, divisor: divisor, amount: amount)
  }
}
public struct NAPCPrice : NAPCPriceProtocol {
  fileprivate struct Keys {
    static let currency = "currency"
    static let divisor = "divisor"
    static let amount = "amount"
  }
  
  public var currency: String
  public var divisor: Int
  public var amount: Int
  
  public init?(dictionary: [String:Any]) {
    if  let currency = dictionary[Keys.currency] as? String,
      let divisor = dictionary[Keys.divisor] as? Int,
      let amount = dictionary[Keys.amount] as? Int {
      self.currency = currency
      self.divisor = divisor
      self.amount = amount
    }
    else {
      return nil
      
    }
  }
  
  // Testing
  public init(currency: String, divisor: Int, amount: Int) {
    self.currency = currency
    self.divisor = divisor
    self.amount = amount
  }
}
