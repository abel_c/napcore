//
//  NAPCBadge.swift
//  NAPClothing
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

public enum NAPCBadge : String {
  case Exclusive = "Exclusive"
  case In_Stock = "In_Stock"
  
  public static func getSet(array: [String]) -> Set<NAPCBadge> {
    var set = Set<NAPCBadge>()
    array.forEach { (badgeString) in
      if let badge = NAPCBadge(rawValue: badgeString) {
        set.insert(badge)
      }
    }
    return set
  }
}

